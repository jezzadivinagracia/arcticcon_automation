*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${homePage}  https://test.arctic-con.com/#/ac2021

###FOOTERDETAILS####
${contactLabel}  xpath=//*[@class="footer__heading smaller-lh"]
${contactText}  Contact us
${contactNumberLine}  xpath=//*[@id="container"]/footer/div[1]/section[2]/span[1]
${contactNumber}   (907) 290-7660
${contactEmailLine}  xpath=//*[@id="container"]/footer/div[1]/section[2]/span[2]
${contactEmail}   info@arctic-con.com
${outputDir}  /Users/rodelabapo/PycharmProjects/arcticcon_automation/Test/Functional/Screenshots/Footer/

###LinkOut####
${cookiePolicy}  xpath=//*[@href="#/cookie-policy"]
${termsOfServices}  xpath=//*[@href="#/terms-of-service"]
${privacyPolicy}  xpath=//*[@href="#/privacy-policy"]

###LINKS####
${linkedInLink}
*** Test Cases ***
###CHROMETEST###
Go to Home Page Chrome
    Home Page Chrome
Verify Contact Us Label
    Contact Us Footer Chrome
Verify Contact Number Used
    Contact Number Footer Chrome
Verify Email Address Used
    Contact Email Footer Chrome
Cookie Policy Link
    Cookie Policy Chrome
Terms of Services Link
    Terms of Services Chrome
Privacy Policy Link
    Privacy Policy Chrome
End Chrome Session
#    Close Chrome Browser

###FIREFOXTEST
#Go to Home Page Firefox
#    Home Page Firefox
#Verify Contact Us Label
#    Contact Us Footer Firefox
#Verify Contact Number Used
#    Contact Number Footer Firefox
#Verify Email Address Used
#    Contact Email Footer Firefox
#Cookie Policy Link
#    Wait For Condition  Verify Email Address Used  3s
#    Cookie Policy Firefox
#Terms of Services Link
#    Terms of Services Firefox
#Privacy Policy Link
#    Privacy Policy Firefox
#End Firefox Session
#    Close Firefox Browser

###SAFARITEST###
#Go to Home Page Safari
#    Home Page Safari
#Cookie Policy Link
#    #Wait For Condition  Verify Email Address Used  3s
#    Cookie Policy Safari
#Terms of Services Link
#    Terms of Services Safari
#Privacy Policy Link
#    Privacy Policy Safari
#Verify Contact Us Label
#    Contact Us Footer Safari
#Verify Contact Number Used
#    Contact Number Footer Safari
#Verify Email Address Used
#    Contact Email Footer Safari
#End Safari Session
#    Close Safari Browser

*** Keywords ***
###CHROME####
Home Page Chrome
    Open Browser  ${homePage}  chrome
    Maximize Browser Window
    Sleep  3s
Contact Us Footer Chrome
    Scroll Element Into View  ${contactLabel}
    Element Text Should Be  ${contactLabel}  ${contactText}
    Capture Page Screenshot  /Users/rodelabapo/PycharmProjects/arcticcon_automation/Test/Functional/Screenshots/Footer//Contactus.png
    Sleep  3s
Contact Number Footer Chrome
    Scroll Element Into View  ${contactNumberLine}
    Element Text Should Be  ${contactNumberLine}  ${contactNumber}
    Capture Page Screenshot  /Users/rodelabapo/PycharmProjects/arcticcon_automation/Test/Functional/Screenshots/Footer//ContactNumber.png
    Sleep  3s
Contact Email Footer Chrome
    Scroll Element Into View  ${contactEmailLine}
    Element Text Should Be  ${contactEmailLine}  ${contactEmail}
    Capture Page Screenshot  /Users/rodelabapo/PycharmProjects/arcticcon_automation/Test/Functional/Screenshots/Footer//ContactEmail.png
    Sleep  3s
Cookie Policy Chrome
    Click Element  ${cookiePolicy}
    Sleep  3s
Terms of Services Chrome
    Click Element  ${termsOfServices}
    Sleep  3s
Privacy Policy Chrome
    Click Element  ${privacyPolicy}
    Sleep  3s
Close Chrome Browser
    Close Browser

###FIREFOX###
Home Page Firefox
    Open Browser  ${homePage}  firefox
    Maximize Browser Window
    Sleep  3s
Contact Us Footer Firefox
    Scroll Element Into View  ${contactLabel}
    Element Text Should Be  ${contactLabel}  ${contactText}
    Capture Page Screenshot  /Users/rodelabapo/PycharmProjects/arcticcon_automation/Test/Functional/Screenshots/Footer//Contactus.png
    Sleep  3s
Contact Number Footer Firefox
    Scroll Element Into View  ${contactNumberLine}
    Element Text Should Be  ${contactNumberLine}  ${contactNumber}
    Capture Page Screenshot  /Users/rodelabapo/PycharmProjects/arcticcon_automation/Test/Functional/Screenshots/Footer//ContactNumber.png
    Sleep  3s
Contact Email Footer Firefox
    Scroll Element Into View  ${contactEmailLine}
    Element Text Should Be  ${contactEmailLine}  ${contactEmail}
    Capture Page Screenshot  /Users/rodelabapo/PycharmProjects/arcticcon_automation/Test/Functional/Screenshots/Footer//ContactEmail.png
    Sleep  3s
Cookie Policy Firefox
    Click Element  ${cookiePolicy}
    Sleep  3s
Terms of Services Firefox
    Click Element  ${termsOfServices}
    Sleep  3s
Privacy Policy Firefox
    Click Element  ${privacyPolicy}
    Sleep  3s
Close Firefox Browser
    Close Browser

###SAFARI####
Home Page Safari
    Open Browser  ${homePage}  safari
    Maximize Browser Window
    Sleep  3s
Contact Number Footer Safari
    Scroll Element Into View  ${contactNumberLine}
    Page Should Contain  ${contactNumber}
    Capture Page Screenshot  /Users/rodelabapo/PycharmProjects/arcticcon_automation/Test/Functional/Screenshots/Footer//ContactNumber.png
    Sleep  3s
Contact Email Footer Safari
    Scroll Element Into View  ${contactEmailLine}
    Page Should Contain  ${contactEmail}
    Capture Page Screenshot  /Users/rodelabapo/PycharmProjects/arcticcon_automation/Test/Functional/Screenshots/Footer//ContactEmail.png
    Sleep  3s
Cookie Policy Safari
    Click Element  ${cookiePolicy}
    Execute Javascript
    Sleep  3s
Terms of Services Safari
    Click Element  ${termsOfServices}
    Sleep  3s
Privacy Policy Safari
    Click Element  ${privacyPolicy}
    Sleep  3s
Contact Us Footer Safari
    Scroll Element Into View  ${contactLabel}
    Element Text Should Be  ${contactLabel}  ${contactText}
    Capture Page Screenshot  /Users/rodelabapo/PycharmProjects/arcticcon_automation/Test/Functional/Screenshots/Footer//Contactus.png
    Sleep  3s
Close Safari Browser
    Close Browser