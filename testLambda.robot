*** Settings ***
Library  SeleniumLibrary
Library  Selenium2Library

#Test Setup  Common.Open test browser
#Test Teardown  Common.Close Test Browser

*** Variables ***

*** Test Cases ***
    [Documentation]  Test via Lambda
    [Tags]  Sanity

Open chrome Browser
    Open test browser
Close Chrome Browser
    Close test browser
*** Keywords ***

Open test browser
    [Timeout]   ${TIMEOUT}
    Open browser  https://lambdatest.github.io/sample-todo-app/  browser=${BROWSER}
    ...  remote_url=${REMOTE_URL}
    ...  desired_capabilities=${CAPABILITIES}

Close test browser
    Run keyword if  '${REMOTE_URL}' != ''
    ...  Report Lambdatest Status
    ...  ${TEST_NAME}
    ...  ${TEST_STATUS}
    Close all browsers