*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${homePage}  https://dev.arctic-con.com/#/boise2022
${btnMenu}  xpath=//*[@class="nav__toggler"]
${applyTalkMenu}  xpath=//*[@id="container"]/nav/div/ul/li[3]/a
${btnCloseMenu}  xpath=//*[@class="nav__toggler active black"]

###APPLYPAGE####
${applyPage}  xpath=//*[@id="apply"]
${btnApply}  xpath=//*[@class="button apply__button"]
${sendApplPage}  xpath=//h2[@class="heading--h2 heading--alter heading--no-decoration heading bg-section__heading"]
${firstName}  xpath=//*[@id="first_name"]
${lastName}  xpath=//*[@id="last_name"]
${organizaion}  xpath=//*[@id="organization"]
${postion}  xpath=//*[@id="job_title"]
${btnNext}  xpath=//*[@class="button speaker-info__button"]
${bio}  xpath=//*[@id="bio"]

###TALKPAGE###
${talkPageTitle}   Talk information
${talkPage}  xpath=//*[@class="talk-info"]
${talkName}  xpath=//*[@id="talk_name"]
${talkTopic}  xpath=//*[@id="container"]/div/div/div[2]/section/form/div[2]/ul/li[1]/div/div[1]
${talkDescription}  xpath=//*[@id="talk_description"]
${btnTalkNext}  xpath=//*[@class="button talk-info__button mt-6"]

###CONTACT###
${contactPage}  xpath=//*[@class="heading--h3 heading--alter heading contact-info__title"]
${contactPageHeader}   How can we contact you?
${contactPhone}  xpath=//*[@id="phone"]
${contactEmail}  xpath=//*[@id="email"]
${contactNext}  xpath=//*[@class="button contact-info__button mt-6"]

###CTPCONFIRMATION###
${ctpConfirmPage}  //*[@class="heading--h1 heading--center heading"]
${ctpConfirmation}   Thank you for your submission!

*** Test Cases ***
####CHROMETEST###
Go to Home Page Chrome
    Home Page Chrome
Create Application
    Select Apply Talk Chrome
    Enter Details Chrome
Create Talk Topic
    Talk Type Chrome
Enter Contacts
    Contact Detaiils Chrome
Confirm CTP Submission
    Confirm Submission Chrome
End Chrome Session
    Close Chrome Browser

###FIREFOXTEST###
Go to Home Page Firefox
    Home Page Firefox
Create Application
    Select Apply Talk Firefox
    Enter Details Firefox
Create Talk Topic
    Talk Type Firefox
Enter Contacts
    Contact Detaiils Firefox
Confirm CTP Submission
    Confirm Submission Firefox
End Firefox Session
    Close Firefox Browser

####SAFARITEST###
Go to Home Page Safari
    Home Page Safari
Create Application
    Select Apply Talk Safari
    Enter Details Safari
Create Talk Topic
    Talk Type Safari
Enter Contacts
    Contact Detaiils Safari
Confirm CTP Submission
    Confirm Submission Safari
End Safari Session
    Close Safari Browser

###OPERA###
Go to Home Page Opera
    Home Page Opera
Create Application
    Select Apply Talk Opera
    Enter Details Opera
Create Talk Topic
    Talk Type Opera
Enter Contacts
    Contact Detaiils Opera
Confirm CTP Submission
    Confirm Submission Opera
End Opera Session
    Close Opera Browser

*** Keywords ***
###CHROME###
Home Page Chrome
    Open Browser  ${homePage}  chrome
    Maximize Browser Window
    Sleep  3s
Select Apply Talk Chrome
    Click Element  ${btnMenu}
    Sleep  3s
    Click Element  ${applyTalkMenu}
    Click Element  ${btnCloseMenu}
    Sleep  3s
    Click Button  ${btnApply}
Enter Details Chrome
    Element Should Be Visible  ${sendApplPage}
    Sleep  3s
    Click Element  ${firstName}
    Input Text  ${firstName}  Sample_FirstName
    Click Element  ${lastName}
    Input Text  ${lastName}  Sample_LastName
    Click Element  ${organizaion}
    Input Text  ${organizaion}  Sample_Organization
    Click Element  ${postion}
    Input Text  ${postion}  Sample_Position
    Click Element  ${bio}
    Input Text  ${bio}  Sample_Bio
    Click Button  ${btnNext}
    Sleep  3s
Talk Type Chrome
    Page Should Contain  ${talkPageTitle}
    Sleep  3s
    Click Element  ${talkName}
    Input Text  ${talkName}  Sample_Talk
    Sleep  2s
    Click Element  ${talkTopic}
    Sleep  2s
    Click Element  ${talkDescription}
    Input Text  ${talkDescription}  Sample Talk Description
    Click Button  ${btnTalkNext}
    Sleep  3s
Contact Detaiils Chrome
    Element Should Be Visible  ${contactPage}
    Element Text Should Be  ${contactPage}  ${contactPageHeader}
    Sleep  3s
    Click Element  ${contactPhone}
    Input Text  ${contactPhone}  8790239809
    Click Element  ${contactEmail}
    Input Text  ${contactEmail}  divinagracia.jezzalabrador@gmail.com
    Sleep  2s
    Click Button  ${contactNext}
    Sleep  3s
Confirm Submission Chrome
    Element Should Be Visible  ${ctpConfirmPage}
    Element Text Should Be  ${ctpConfirmPage}  ${ctpConfirmation}
Close Chrome Browser
    Close Browser

####FIREFOX####
Home Page Firefox
    Open Browser  ${homePage}  firefox
    Maximize Browser Window
    Sleep  3s
Select Apply Talk Firefox
    Click Element  ${btnMenu}
    Sleep  3s
    Click Element  ${applyTalkMenu}
    Click Element  ${btnCloseMenu}
    Sleep  3s
    Click Button  ${btnApply}
Enter Details Firefox
    Element Should Be Visible  ${sendApplPage}
    Sleep  3s
    Click Element  ${firstName}
    Input Text  ${firstName}  Sample_FirstName
    Click Element  ${lastName}
    Input Text  ${lastName}  Sample_LastName
    Click Element  ${organizaion}
    Input Text  ${organizaion}  Sample_Organization
    Click Element  ${postion}
    Input Text  ${postion}  Sample_Position
    Click Element  ${bio}
    Input Text  ${bio}  Sample_Bio
    Click Button  ${btnNext}
    Sleep  3s
Talk Type Firefox
    Page Should Contain  ${talkPageTitle}
    Sleep  3s
    Click Element  ${talkName}
    Input Text  ${talkName}  Sample_Talk
    Sleep  2s
    Click Element  ${talkTopic}
    Sleep  2s
    Click Element  ${talkDescription}
    Input Text  ${talkDescription}  Sample Talk Description
    Click Button  ${btnTalkNext}
    Sleep  3s
Contact Detaiils Firefox
    Element Should Be Visible  ${contactPage}
    Element Text Should Be  ${contactPage}  ${contactPageHeader}
    Sleep  3s
    Click Element  ${contactPhone}
    Input Text  ${contactPhone}  8790239809
    Click Element  ${contactEmail}
    Input Text  ${contactEmail}  divinagracia.jezzalabrador@gmail.com
    Sleep  2s
    Click Button  ${contactNext}
    Sleep  3s
Confirm Submission Firefox
    Element Should Be Visible  ${ctpConfirmPage}
    Element Text Should Be  ${ctpConfirmPage}  ${ctpConfirmation}
Close Firefox Browser
    Close Browser

####SAFARI####
Home Page Safari
    Open Browser  ${homePage}  safari
    Maximize Browser Window
    Sleep  3s
Select Apply Talk Safari
    Click Element  ${btnMenu}
    Sleep  3s
    Click Element  ${applyTalkMenu}
    Click Element  ${btnCloseMenu}
    Sleep  3s
    Click Button  ${btnApply}
Enter Details Safari
    Element Should Be Visible  ${sendApplPage}
    Sleep  3s
    Click Element  ${firstName}
    Input Text  ${firstName}  Sample_FirstName
    Click Element  ${lastName}
    Input Text  ${lastName}  Sample_LastName
    Click Element  ${organizaion}
    Input Text  ${organizaion}  Sample_Organization
    Click Element  ${postion}
    Input Text  ${postion}  Sample_Position
    Click Element  ${bio}
    Input Text  ${bio}  Sample_Bio
    Click Button  ${btnNext}
    Sleep  3s
Talk Type Safari
    Page Should Contain  ${talkPageTitle}
    Sleep  3s
    Click Element  ${talkName}
    Input Text  ${talkName}  Sample_Talk
    Sleep  2s
    Click Element  ${talkTopic}
    Sleep  2s
    Click Element  ${talkDescription}
    Input Text  ${talkDescription}  Sample Talk Description
    Click Button  ${btnTalkNext}
    Sleep  3s
Contact Detaiils Safari
    Element Should Be Visible  ${contactPage}
    Page Should Contain  ${contactPageHeader}
    Sleep  3s
    Click Element  ${contactPhone}
    Input Text  ${contactPhone}  8790239809
    Click Element  ${contactEmail}
    Input Text  ${contactEmail}  divinagracia.jezzalabrador@gmail.com
    Sleep  2s
    Click Button  ${contactNext}
    Sleep  3s
Confirm Submission Safari
    Element Should Be Visible  ${ctpConfirmPage}
    Page Should Contain  ${ctpConfirmation}
Close Safari Browser
    Close Browser

###OPERA###
Home Page Opera
    Open Browser  ${homePage}  opera
    Maximize Browser Window
    Sleep  3s
Select Apply Talk Opera
    Click Element  ${btnMenu}
    Sleep  3s
    Click Element  ${applyTalkMenu}
    Click Element  ${btnCloseMenu}
    Sleep  3s
    Click Button  ${btnApply}
Enter Details Opera
    Element Should Be Visible  ${sendApplPage}
    Sleep  3s
    Click Element  ${firstName}
    Input Text  ${firstName}  Sample_FirstName
    Click Element  ${lastName}
    Input Text  ${lastName}  Sample_LastName
    Click Element  ${organizaion}
    Input Text  ${organizaion}  Sample_Organization
    Click Element  ${postion}
    Input Text  ${postion}  Sample_Position
    Click Element  ${bio}
    Input Text  ${bio}  Sample_Bio
    Click Button  ${btnNext}
    Sleep  3s
Talk Type Opera
    Page Should Contain  ${talkPageTitle}
    Sleep  3s
    Click Element  ${talkName}
    Input Text  ${talkName}  Sample_Talk
    Sleep  2s
    Click Element  ${talkTopic}
    Sleep  2s
    Click Element  ${talkDescription}
    Input Text  ${talkDescription}  Sample Talk Description
    Click Button  ${btnTalkNext}
    Sleep  3s
Contact Detaiils Opera
    Element Should Be Visible  ${contactPage}
    Element Text Should Be  ${contactPage}  ${contactPageHeader}
    Sleep  3s
    Click Element  ${contactPhone}
    Input Text  ${contactPhone}  8790239809
    Click Element  ${contactEmail}
    Input Text  ${contactEmail}  divinagracia.jezzalabrador@gmail.com
    Sleep  2s
    Click Button  ${contactNext}
    Sleep  3s
Confirm Submission Opera
    Element Should Be Visible  ${ctpConfirmPage}
    Element Text Should Be  ${ctpConfirmPage}  ${ctpConfirmation}
Close Opera Browser
    Close Browser
