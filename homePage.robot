*** Settings ***
Library  SeleniumLibrary

*** Variables ***
##Check CountDown###
###${homePage}  https://arctic-con.com/#/home
${homePage}  https://d35aodsnc0daas.cloudfront.net/#/unicorncon2022
${countDown}  xpath=//*[@id="home"]/div/div/div/div/div/ul
${chromePath}  /Users/rodelabapo/PycharmProjects/Arctic_V01/Test/Functional/Screenshots/chrome
${safariPath}  /Users/rodelabapo/PycharmProjects/Arctic_V01/Test/Functional/Screenshots/Safari
${firefoxPath}  /Users/rodelabapo/PycharmProjects/Arctic_V01/Test/Functional/Screenshots/Firefox
${operaPath}  /Users/rodelabapo/PycharmProjects/Arctic_V01/Test/Functional/Screenshots/Opera


###EVENT REGISTRATION###
${btnRegistration}  xpath=//*[@class="btn accent btn-secondary"]
${registrationPage}  xpath=//*[@id="app"]/section/div/div/div/section/div/div/div/div/div
${emailAddress}  xpath=//*[@id="email"]
${btnContinue}  xpath=//*[@id="codeBtn"]
*** Test Cases ***
#Test Count Down Visibility in Chrome Browser
#    Check CountDown Visibility using Chrome Browser
Select Register Button in Chrome Browser
    Register using Chrome Browser
#Test Count Down Visibility in Firefox Browser
#    Check CountDown Visibility using Firefox Browser
#Select Register Button in Firefox
#    Register using Firefox Browser
#Test Count Down Visibility in Safari Browser
#    Check CountDown Visibility using Safari Browser
#Select Register Button in Safari
#    Register using Safari Browser
#Test Count Down Visibility in Opera Browser
#    Check CountDown Visibility using Opera Browser
#Select Register Button in Opera
#    Register using Opera Browser

*** Keywords ***
###CHROMEBROWSER###
Check CountDown Visibility using Chrome Browser
    Open Browser  ${homePage}  chrome
    Maximize Browser Window
#CountDown Check in Chrome
    Set Browser Implicit Wait  10
    Wait Until Element Is Visible  ${countDown}  10
    Page Should Contain  Text  ArcticCon 2021
#Capture CountDown ScreenShot
    Set Browser Implicit Wait  10
    Set Screenshot Directory  ${chromePath}
    Capture Page Screenshot  selenium-element-screenshot-{index}.png
Register using Chrome Browser
    Click Button  ${btnRegistration}
    Wait Until Element is Visible  ${registrationPage}
    Set Browser Implicit Wait  10
    Set Screenshot Directory  ${chromePath}
    Capture Page Screenshot  selenium-element-screenshot-{index}.png
    Click Element  ${emailAddress}
    Input Text  ${emailAddress}  test@gmail.com
    Click Button  ${btnContinue}
#Close Chrome Browser
    Set Browser Implicit Wait  10
    Close Browser

###FIREFOX####
Check CountDown Visibility using Firefox Browser
    Open Browser  ${homePage} firefox
    Maximize Browser Window
#CountDown Check in Firefox
    Set Browser Implicit Wait  10
    Wait Until Element Is Visible  ${countDown}  10
    Page Should Contain  Text  ArcticCon 2021
#Capture CountDown ScreenShot
    Set Screenshot Directory  ${firefoxPath}
    Capture Page Screenshot  selenium-element-screenshot-{index}.png
Register using Firefox Browser
    Click Button  ${btnRegistration}
    Wait Until Element is Visible  ${registrationPage}
    Set Browser Implicit Wait  10
    Set Screenshot Directory  ${firefoxPath}
    Capture Page Screenshot  selenium-element-screenshot-{index}.png
    Click Element  ${emailAddress}
    Input Text  ${emailAddress}  test@gmail.com
    Click Button  ${btnContinue}
#Close Firefox Browser
    Set Browser Implicit Wait  10
    Close Browser

###SAFARI###
Check CountDown Visibility using Safari Browser
    Open Browser  ${homePage}  safari
    Maximize Browser Window
#CountDown Check in Safari
    Set Browser Implicit Wait  10
    Wait Until Element Is Visible  ${countDown}  10
    Page Should Contain  Text  ArcticCon 2021
#Capture CountDown ScreenShot
    Set Screenshot Directory  ${safariPath}
    Capture Page Screenshot  selenium-element-screenshot-{index}.png
Register using Safari Browser
    Click Button  ${btnRegistration}
    Wait Until Element is Visible  ${registrationPage}
    Set Browser Implicit Wait  10
    Set Screenshot Directory  ${safariPath}
    Capture Page Screenshot  selenium-element-screenshot-{index}.png
    Click Element  ${emailAddress}
    Input Text  ${emailAddress}  test@gmail.com
    Click Button  ${btnContinue}
#Close Safari Browser
    Set Browser Implicit Wait  20
    Close Browser

###OPERA####
Check CountDown Visibility using Opera Browser
    Open Browser  ${homePage}  opera
    Maximize Browser Window
#CountDown Check in Opera
    Set Browser Implicit Wait  10
    Wait Until Element is Visible  ${countDown}  10
    Page should Contain  Text  ArcticCon 2021
#Capture CountDown ScreenShot
    Set Screenshot Directory  ${operaPath}
    Capture Page Screenshot  selenium-element-screenshot-{index}.png
    Sleep  5s
Register using Opera Browser
    Click Button  ${btnRegistration}
    Wait Until Element is Visible  ${registrationPage}
    Set Browser Implicit Wait  10
    Set Screenshot Directory  ${operaPath}
    Capture Page Screenshot  selenium-element-screenshot-{index}.png
    Click Element  ${emailAddress}
    Input Text  ${emailAddress}  test@gmail.com
    Click Button  ${btnContinue}
#Close Opera Browser
    Set Browser Implicit Wait  10
    Close Browser


