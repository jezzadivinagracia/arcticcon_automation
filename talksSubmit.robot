*** Settings ***
Library  SeleniumLibrary

*** Variables ***
###Go to Application###
${homePage}  https://arctic-con.com/#/home
${mnuTalks}  xpath=//*[@data-hover="Talks"]
${btnApply}  xpath=//*[@type="button" and @class="btn my-1 text-center accent btn-secondary"]
${countDown}  xpath=//*[@id="home"]/div/div/div/div/div/ul
${mnuAbout}  xpath=//*[@data-hover="About"]
###General Information Page###
${genInfoPage}  xpath=//*[@id="app"]/section/div/div/div/div/section/div/div/div/h2[1]
${firstName}  xpath=//*[@id="first_name"]
${lastName}  xpath=//*[@id="last_name"]
${email}  xpath=//*[@id="email"]
${phone}  xpath=//*[@id="phone"]
${organization}  xpath=//*[@id="organization"]
${bio}  xpath=//*[@id="bio"]
${title}  xpath=//*[@id="title"]
${description}  xpath=//*[@id="description"]
${talkType}  xpath=//*[@id="talk_type"]
${talkOptions}  xpath=//*[@id="talk_type"]/option[3]
${btnSubmit}  xpath=//*[@id="submitBtn"]
##Submit Sreen Shot###
${submitPath}  /Users/rodelabapo/PycharmProjects/Arctic_V01/Test/Functional/Screenshots/Submit

*** Test Cases ***
    [Documentation]  This is to Test Application Submission for Talks
    [Tags]  Regression

##CHROME##
#Go to Home Page using Chrome
#    Home page
#Go to Talk Application Page
#    Application Page
#Fill Up General Details
#    General Information
#Select Talk Details
#    Talk Details
#End Chrome Session
#    Close Chrome Browser
#
###FIREFOX
#Go to Homepage using FIrefox
#    Home page using Firefox
#Go to Application page
#    Application Page on Firefox
#Add General Details
#    General Information on Firefox
#Select Talk Type
#    Talk Details on Firefox
#End Firefox Session
#    Close Firefox Browser

###SAFARI
#Go to Home Page using Safari
#    Home page using Safari
#Go to Application page
#    Application Page on Safari
#Add General Details
#    General Information on Safari
#Add Talk Type
#    Talk Details on Safari
#End Safari Session
#    Close Safari Browser

##OPERA##
Go to Home Page using Opera
    Home page using Opera
Go to Application Page
    Application Page on Opera
Add General Details
    General Information on Opera
Add Talk Type
    Talk Details on Opera
End Opera Session
    Close Opera Browser

*** Keywords ***
##CHROME##
Home page using Chrome
    Open Browser  ${homepage}  chrome
    Maximize Browser Window
    Sleep  5s
    Click Element  ${mnuTalks}
    Sleep  3s
Application Page
    Scroll Element Into View  ${btnApply}
    Set Browser Implicit Wait  1m
    Click Button  ${btnApply}
    Wait Until Element Is Visible  ${genInfoPage}
General Information
    Click Element  ${firstName}
    Input Text  ${firstName}  firstName
    Click Element  ${lastName}
    Input Text  ${lastName}  lastName
    Click Element  ${email}
    Input Text  ${email}  test@gmail.com
    Click Element  ${phone}
    Input Text  ${phone}  09123456789
    Click Element  ${bio}
    Input Text  ${bio}  This is a biography Content
    Click Element  ${organization}
    Input Text  ${organization}  Threat Informant
Talk Details
    Click Element  ${title}
    Input Text  ${title}  Automation Tester
    Click Element  ${description}
    Input Text  ${description}  This is a description
    Sleep  2s
    Click Element  ${talkType}
    Sleep  3s
    Mouse Over  ${talkType}
    Sleep  2s
    Select From List By Label  ${talkType}  Build It
    Click Element  ${talkOptions}
    Sleep  2
    [Arguments]
    Set Screenshot Directory  ${submitPath}
    Capture Page Screenshot  selenium-element-screenshot-{index}.png
    Sleep  2
    Click Button  ${btnSubmit}
    Sleep  3
    #[Arguments]
    Set Screenshot Directory  ${submitPath}
    Capture Page Screenshot  selenium-element-screenshot-{index}.png
Close Chrome Browser
    Close Browser

##FIREFOX##
Home page using Firefox
    Open Browser  ${homepage}  firefox
    Maximize Browser Window
    Sleep  5s
    Click Element  ${mnuTalks}
    Sleep  3s
Application Page on Firefox
    Scroll Element Into View  ${btnApply}
    Set Browser Implicit Wait  1m
    Click Button  ${btnApply}
    Wait Until Element Is Visible  ${genInfoPage}
General Information on Firefox
    Click Element  ${firstName}
    Input Text  ${firstName}  firstName
    Click Element  ${lastName}
    Input Text  ${lastName}  lastName
    Click Element  ${email}
    Input Text  ${email}  test@gmail.com
    Click Element  ${phone}
    Input Text  ${phone}  09123456789
    Click Element  ${bio}
    Input Text  ${bio}  This is a biography Content
    Click Element  ${organization}
    Input Text  ${organization}  Threat Informant
Talk Details on Firefox
    Click Element  ${title}
    Input Text  ${title}  Automation Tester
    Click Element  ${description}
    Input Text  ${description}  This is a description
    Sleep  2s
    Click Element  ${talkType}
    Sleep  3s
    Mouse Over  ${talkType}
    Sleep  2s
    Select From List By Label  ${talkType}  Build It
    Click Element  ${talkOptions}
    Sleep  3s
    [Arguments]
    Set Screenshot Directory  ${submitPath}
    Capture Page Screenshot  selenium-element-screenshot-{index}.png
    Sleep  3s
    Click Button  ${btnSubmit}
    Sleep  3s
    #[Arguments]
    Set Screenshot Directory  ${submitPath}
    Capture Page Screenshot  selenium-element-screenshot-{index}.png
Close Firefox Browser
    Close Browser


 ##SAFARI##
Home page using Safari
    Open Browser  ${homepage}  safari
    Maximize Browser Window
    Sleep  10s
    Click Element  ${mnuTalks}
    Sleep  5s
Application Page on Safari
    Scroll Element Into View  ${btnApply}
    Set Browser Implicit Wait  10s
    Click Button  ${btnApply}
    Wait Until Element Is Visible  ${genInfoPage}
General Information on Safari
    Click Element  ${firstName}
    Input Text  ${firstName}  firstName
    Click Element  ${lastName}
    Input Text  ${lastName}  lastName
    Click Element  ${email}
    Input Text  ${email}  test@gmail.com
    Click Element  ${phone}
    Input Text  ${phone}  09123456789
    Click Element  ${bio}
    Input Text  ${bio}  This is a biography Content
    Click Element  ${organization}
    Input Text  ${organization}  Threat Informant
Talk Details on Safari
    Click Element  ${title}
    Input Text  ${title}  Automation Tester
    Click Element  ${description}
    Input Text  ${description}  This is a description
    Sleep  2s
    Click Element  ${talkType}
    Sleep  3s
    Mouse Over  ${talkType}
    Sleep  2s
    Select From List By Label  ${talkType}  Build It
    Click Element  ${talkOptions}
    Sleep  3s
    [Arguments]
    Set Screenshot Directory  ${submitPath}
    Capture Page Screenshot  selenium-element-screenshot-{index}.png
    Sleep  3s
    Click Button  ${btnSubmit}
    Sleep  3s
    #[Arguments]
    Set Screenshot Directory  ${submitPath}
    Capture Page Screenshot  selenium-element-screenshot-{index}.png
Close Safari Browser
    Close Browser

##OPERA##
Home page using Opera
    Open Browser  ${homepage}  opera
    Maximize Browser Window
    Sleep  10s
    Click Element  ${mnuTalks}
    Sleep  5s
Application Page on Opera
    Scroll Element Into View  ${btnApply}
    Set Browser Implicit Wait  10s
    Click Button  ${btnApply}
    Wait Until Element Is Visible  ${genInfoPage}
General Information on Opera
    Click Element  ${firstName}
    Input Text  ${firstName}  firstName
    Click Element  ${lastName}
    Input Text  ${lastName}  lastName
    Click Element  ${email}
    Input Text  ${email}  test@gmail.com
    Click Element  ${phone}
    Input Text  ${phone}  09123456789
    Click Element  ${bio}
    Input Text  ${bio}  This is a biography Content
    Click Element  ${organization}
    Input Text  ${organization}  Threat Informant
Talk Details on Opera
    Click Element  ${title}
    Input Text  ${title}  Automation Tester
    Click Element  ${description}
    Input Text  ${description}  This is a description
    Sleep  2s
    Click Element  ${talkType}
    Sleep  3s
    Mouse Over  ${talkType}
    Sleep  2s
    Select From List By Label  ${talkType}  Build It
    Click Element  ${talkOptions}
    Sleep  3s
    [Arguments]
    Set Screenshot Directory  ${submitPath}
    Capture Page Screenshot  selenium-element-screenshot-{index}.png
    Sleep  3s
    Click Button  ${btnSubmit}
    Sleep  3s
    #[Arguments]
    Set Screenshot Directory  ${submitPath}
    Capture Page Screenshot  selenium-element-screenshot-{index}.png
Close Opera Browser
    Close Browser


